#!/bin/zsh

# Good article about building an install: https://josephephillips.com/mac-setup/

# list out brew apps to install
typeset -a brewAppsAll; typeset -a brewAppsHome; typeset -a brewAppsHomeCask; typeset -a brewAppsWork;
brewAppsAll=( bat httpie jq pandoc rename tree watch yq wget);
brewAppsHome=( python3 );
brewAppsHomeCask=( abricotine zettlr qlmarkdown microsoft-edge betterzip font-fira-code );
# possible casks: iterm2 visual-studio-code 1password macdown qlmarkdown
brewAppsWork=();

# Brew install & add cask repo

#brew install --cask font-fira-code

# list out vscode extensions to install
typeset -a codeExtAll; typeset -a codeExtWork; typeset -a codeExtHome;
# Extensions that we don't want installed for one reason or another
codeExtRemove=(
  batisteo.vscode-django # Remove - Work - License: None
  formulahendry.terminal # Remove - deprecated
  hnw.vscode-auto-open-markdown-preview # Remove - Don't like it
  nickdemayo.vscode-json-editor # Remove - Work - License: None
  sissel.shopify-liquid # Remove - Work - No SF license: CC-by-nc-nd/4.0
  tomblind.scm-buttons-vscode # Remove - Work - license: non-standard
  visualStudioExptTeam.vscodeintellicode # Remove - Work - License: MS Software License
  ginfuru.ginfuru-vscode-jekyll-syntax # Remove - depredated, use sissel.shopify-liquid instead
  mhutchie.git-graph # Remove - License: non-standard
);
codeExtAll=(
  tobiastao.vscode-md # License: MIT
  2Sherpas.2sherpas # License: MIT
  abdillah.belikejekyll # License: Apache 2.0
  alefragnani.Bookmarks # License: MIT
  alefragnani.project-manager # License: MIT
  alexcvzz.vscode-sqlite # License: Apache 2.0
  amariampolskiy.theme-apprentice # License: MIT
  auchenberg.vscode-browser-preview # License: MIT
  bierner.github-markdown-preview # License: MIT
  bierner.markdown-checkbox # License: MIT
  bierner.markdown-emoji # License: MIT
  bierner.markdown-mermaid # License: MIT
  bierner.markdown-preview-github-styles # License: MIT
  bierner.markdown-yaml-preamble # License: MIT
  chukwuamaka.csvtojson-converter # License: MIT
  codezombiech.gitignore # License: MIT
  davidAnson.vscode-markdownlint # License: MIT
  davidanson.vscode-markdownlint # License: MIT
  dedsec727.jekyll-run # License: MIT
  donjayamanne.git-extension-pack # License: MIT
  donjayamanne.githistory # License: MIT
  donjayamanne.python-extension-pack # License: MIT
  dracula-theme.theme-dracula # License: MIT
  eamodio.gitlens # License: MIT
  eliostruyf.vscode-front-matter # License: MIT
  felipecaputo.git-project-manager # License: MIT
  felixfbecker.php-intellisense # License: MIT
  formulahendry.code-runner # License: MIT
  formulahendry.docker-explorer # License: MIT
  george3447.docker-run # License: MIT
  ginfuru.vscode-jekyll-snippets # License: MIT
  gruntfuggly.todo-tree # License: MIT
  hashicorp.terraform # License: Mozilla PL 2.0
  ionutvmi.path-autocomplete # License: MIT
  jeepshen.vscode-markdown-code-runner # License: MIT
  kevinRose.vsc-python-indent # License: MIT
  magicstack.MagicPython # License: MIT
  markdownTablePrettify-VSCodeExt # License: MIT
  mechatroner.rainbow-csv # License: MIT
  ms-azuretools.vscode-docker # License: MIT
  ms-python.python # License: MIT
  mukundan.python-docs # License: MIT
  oderwat.indent-rainbow # License: MIT
  osteele.jekyll # License: MIT
  pascalReitermann93.vscode-yaml-sort # License: MIT
  pkief.material-icon-theme # License: MIT
  redhat.vscode-yaml # License: MIT
  rohgarg.jekyll-post # License: Apache 2.0
  tht13.python # License: MIT
  tobiasTao.vscode-md # License: MIT
  tombonnike.vscode-status-bar-format-toggle # License: MIT
  tushortz.python-extended-snippets # License: MIT
  vscode-markdown-checkboxes # License: MIT
  vscodevim.vim # License: MIT
  vscoss.vscode-ansible # License: MIT
  wayou.vscode-todo-highlight # License: MIT
  wholroyd.jinja # License: MIT
  yzhang.markdown-all-in-one # License: MIT
  zainChen.json # License: MIT
  zhuangtongfa.material-theme # License: MIT
  ziyasal.vscode-open-in-github # License: MIT
);
codeExtHome=(
  ms-toolsai.jupyter # License: MS Pre-release Software License
  ms-vscode-remote.remote-containers # License: MS Pre-release Software License
  ms-vscode-remote.remote-ssh # License: MS Pre-release Software License
  ms-vscode-remote.remote-ssh-edit # License: MS Pre-release Software License
  mhutchie.git-graph # Remove - License: non-standard
);
codeExtWork=(
);


# begin writing file of steps to do after install
postInstall="~/PostInstall.md"
echo "# Steps After Install" > $postInstall

location="home"

if [ "$USER" = "tptu" ]; then
  echo "Use work (SF) setup"
  location="work"
fi;

echo "LOCATION: $location"

# add extra packages based on location
if [ "$location" = "work" ]; then
  brewAppsAll+=($brewAppsWork)
  codeExtAll+=($codeExtWork)
fi;
if [ "$location" = "home" ]; then
  brewAppsAll+=($brewAppsHome)
  codeExtAll+=($codeExtHome)
fi;

# if work use proxy
if [ "$location" = "work" ]; then
  echo "WORK setup"
  # start the proxy
  echo "make sure proxy script is setup first"
  echo "turning proxy on"
  proxy_on
fi;

####### homebrew ############
if test ! %(which brew); then
  echo "Installing homebrew"
  /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
fi;

# update brew install
brew update

# install our brew apps
brew install $brewAppsAll[@];

# add the cask-fonts tap
brew tap homebrew/cask-fonts

# for home install our cask apps
if [ "$location" = "home" ]; then
  # install our cask apps
  brew install --cask $brewAppsHomeCask[@];
fi;

brew cleanup

####### /homebrew ###############

# Add interactive comments to allow "ls -la # this is a ls command" style comments
# TODO: move this into .zprofile in linux setup
echo "setopt interactivecomments" >> ~/.zprofile

###### VS Code ######
echo "install VS Code"
curl -fL "https://code.visualstudio.com/sha/download?build=stable&os=darwin" --output ~/Downloads/vsCode.zip
# extract & install to Applications
rm -rf "/Applications/Visual Studio Code.app"
unzip ~/Downloads/vsCode.zip -d /Applications/

# Setup the "code" command line to the path on zsh
cat << EOF >> ~/.zprofile
# Add Visual Studio Code (code)
export PATH="\$PATH:/Applications/Visual Studio Code.app/Contents/Resources/app/bin"
EOF

# source our zsh profile to get our path
source ~/.zprofile

# loop through & remove the extensions we don't want
for ext ("$codeExtRemove[@]");
do
  # for now we just output a line to the todo list
  echo "- [ ] Remove VS Code Extension: $ext" >> $postInstall
done

# loop through & install all our extensions
for ext ("$codeExtAll[@]");
do
  code --force --install-extension $ext
done
####### /VS Code #######


# install python
#brew install python3

# Install PIP and Ansible following Ansible instructions
# https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html#from-pip

## Install PIP
#curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
#sudo python get-pip.py

## Install Ansible
#sudo pip install ansible


# Install Brew Cask
#ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)" < /dev/null 2> /dev/null ; brew install caskroom/cask/brew-cask 2> /dev/null




# for work turn proxy back off
if [ "$location" = "work" ]; then
  echo "WORK setup"
  # start the proxy
  echo "turn off proxy"
  proxy_off
fi;

# set a few OS changes

echo "Configuring OS..."

# Set key repeat - We need to enable this in order to use VI normally in VS Code
defaults write com.microsoft.VSCode ApplePressAndHoldEnabled -bool false
# Set fast key repeat rate
defaults write NSGlobalDomain KeyRepeat -int 0
# Require password as soon as screensaver or sleep mode starts
defaults write com.apple.screensaver askForPassword -int 1
defaults write com.apple.screensaver askForPasswordDelay -int 0
# Show filename extensions by default
defaults write NSGlobalDomain AppleShowAllExtensions -bool true
# Enable tap-to-click
defaults write com.apple.driver.AppleBluetoothMultitouch.trackpad Clicking -bool true
defaults -currentHost write NSGlobalDomain com.apple.mouse.tapBehavior -int 1
# show hidden files
defaults write com.apple.Finder AppleShowAllFiles true

# reload finder after changes
killall Finder
